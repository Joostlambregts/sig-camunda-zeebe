package nl.amis.sigcamunda.loanworkflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoanworkflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoanworkflowApplication.class, args);
	}

}
