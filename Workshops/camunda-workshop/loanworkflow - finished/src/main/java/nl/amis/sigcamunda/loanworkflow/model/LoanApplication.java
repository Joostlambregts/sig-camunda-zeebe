package nl.amis.sigcamunda.loanworkflow.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Data
public class LoanApplication implements Serializable {
    private double amount;
    private String applicant;
    private boolean hasGoodCreditRating;
    private String id;
}
