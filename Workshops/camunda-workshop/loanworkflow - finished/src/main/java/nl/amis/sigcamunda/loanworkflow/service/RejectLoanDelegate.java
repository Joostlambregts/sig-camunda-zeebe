package nl.amis.sigcamunda.loanworkflow.service;

import nl.amis.sigcamunda.loanworkflow.model.LoanApplication;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service
public class RejectLoanDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        LoanApplication loanApplication = (LoanApplication)delegateExecution.getVariable("loanApplication");
        rejectLoan(loanApplication);
    }

    private void rejectLoan(LoanApplication loanApplication){
        System.out.println("rejected loan: " + loanApplication.toString());
    }
}
