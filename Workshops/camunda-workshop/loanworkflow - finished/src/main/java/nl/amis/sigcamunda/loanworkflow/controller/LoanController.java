package nl.amis.sigcamunda.loanworkflow.controller;

import nl.amis.sigcamunda.loanworkflow.model.LoanApplication;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@RestController("/services")
public class LoanController {
    @Autowired
    private RuntimeService runtimeService;

    AtomicLong loanId = new AtomicLong(1);

    public String newLoanId() {return Long.toString(loanId.getAndIncrement());}

    @PostMapping("/services/loan")
    public String createLoanApplication(@RequestBody LoanApplication loanApplication){
        //genereer nieuw id
        String loanId = newLoanId();
        loanApplication.setId(loanId);

        //Maak variable map met één variabele 'order'
        Map<String,Object> variableMap = new HashMap<>();
        variableMap.put("loanApplication", loanApplication);

        //Start order-process instance met orderId als business key en order als variabele
        runtimeService.startProcessInstanceByKey("loan-process", loanId, variableMap);

        return loanId;
    }

    @PutMapping("/services/loan-processed/{loanId}")
    public void receiveLoanProcessedMessage(@PathVariable("loanId") String loanId){
        runtimeService.correlateMessage("loan-processed",loanId);
    }
}
