package nl.amis.sigcamunda.loanworkflow.model;

import java.io.Serializable;

public class LoanApplication implements Serializable {
    private double amount;
    private String applicant;
    private boolean hasGoodCreditRating;
    private String id;

    public LoanApplication() {
    }

    public double getAmount() {
        return this.amount;
    }

    public String getApplicant() {
        return this.applicant;
    }

    public boolean isHasGoodCreditRating() {
        return this.hasGoodCreditRating;
    }

    public String getId() {
        return this.id;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public void setHasGoodCreditRating(boolean hasGoodCreditRating) {
        this.hasGoodCreditRating = hasGoodCreditRating;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof LoanApplication)) return false;
        final LoanApplication other = (LoanApplication) o;
        if (!other.canEqual((Object) this)) return false;
        if (Double.compare(this.getAmount(), other.getAmount()) != 0) return false;
        final Object this$applicant = this.getApplicant();
        final Object other$applicant = other.getApplicant();
        if (this$applicant == null ? other$applicant != null : !this$applicant.equals(other$applicant)) return false;
        if (this.isHasGoodCreditRating() != other.isHasGoodCreditRating()) return false;
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof LoanApplication;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $amount = Double.doubleToLongBits(this.getAmount());
        result = result * PRIME + (int) ($amount >>> 32 ^ $amount);
        final Object $applicant = this.getApplicant();
        result = result * PRIME + ($applicant == null ? 43 : $applicant.hashCode());
        result = result * PRIME + (this.isHasGoodCreditRating() ? 79 : 97);
        final Object $id = this.getId();
        result = result * PRIME + ($id == null ? 43 : $id.hashCode());
        return result;
    }

    public String toString() {
        return "LoanApplication(amount=" + this.getAmount() + ", applicant=" + this.getApplicant() + ", hasGoodCreditRating=" + this.isHasGoodCreditRating() + ", id=" + this.getId() + ")";
    }
}
