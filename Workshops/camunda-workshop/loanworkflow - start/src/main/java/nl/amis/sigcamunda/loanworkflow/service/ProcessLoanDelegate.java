package nl.amis.sigcamunda.loanworkflow.service;

import nl.amis.sigcamunda.loanworkflow.model.LoanApplication;
import org.springframework.stereotype.Service;

@Service
public class ProcessLoanDelegate {

    private void processLoan(LoanApplication loanApplication){
        System.out.println("processed loan: " + loanApplication.toString());
    }
}
