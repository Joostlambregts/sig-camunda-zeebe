package nl.amis.sigcamunda.loanworkflow.controller;

import nl.amis.sigcamunda.loanworkflow.model.LoanApplication;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;

@RestController("/services")
public class LoanController {
    AtomicLong loanId = new AtomicLong(1);

    public String newLoanId() {return Long.toString(loanId.getAndIncrement());}

    @PostMapping("/services/loan")
    public String createLoanApplication(@RequestBody LoanApplication loanApplication){
        //genereer nieuw id
        String loanId = newLoanId();
        loanApplication.setId(loanId);

        //todo

        return loanId;
    }

    @PutMapping("/services/loan-processed/{loanId}")
    public void receiveLoanProcessedMessage(@PathVariable("loanId") String loanId){
        //todo
    }
}
