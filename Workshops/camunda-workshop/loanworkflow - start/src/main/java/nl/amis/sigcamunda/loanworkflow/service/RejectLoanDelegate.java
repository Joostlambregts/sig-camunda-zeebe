package nl.amis.sigcamunda.loanworkflow.service;

import nl.amis.sigcamunda.loanworkflow.model.LoanApplication;
import org.springframework.stereotype.Service;

@Service
public class RejectLoanDelegate {
    private void rejectLoan(LoanApplication loanApplication){
        System.out.println("rejected loan: " + loanApplication.toString());
    }
}
