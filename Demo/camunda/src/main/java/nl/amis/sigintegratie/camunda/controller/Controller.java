package nl.amis.sigintegratie.camunda.controller;

import nl.amis.sigintegratie.camunda.model.Order;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@RestController("/services")
public class Controller {
    @Autowired
    private RuntimeService runtimeService;

    AtomicLong orderId = new AtomicLong(1);

    public long newOrderId(){
        return orderId.getAndIncrement();
    }

    @PostMapping("/services/order")
    public long createOrder(@RequestBody Order order){
        //genereer nieuw id
        long orderId = newOrderId();
        order.setId(orderId);

        //Maak variable map met één variabele 'order'
        Map<String,Object> variableMap = new HashMap<>();
        variableMap.put("order", order);

        //Start order-process instance met orderId als business key en order als variabele
        runtimeService.startProcessInstanceByKey("order-process", Long.toString(orderId), variableMap);
        return orderId;
    }

    @PutMapping("/services/payment-received/{orderId}")
    public void receivePaymentReceivedMessage(@PathVariable("orderId") String orderId){
        runtimeService.correlateMessage("payment-received",orderId);
    }

    @PutMapping("/services/goods-delivered/{orderId}")
    public void receiveGoodsDeliveredMessage(@PathVariable("orderId") String orderId){
        runtimeService.correlateMessage("goods-delivered",orderId);
    }
}
