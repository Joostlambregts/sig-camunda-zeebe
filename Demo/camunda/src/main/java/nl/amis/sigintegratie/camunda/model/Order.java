package nl.amis.sigintegratie.camunda.model;

import java.io.Serializable;

public class Order implements Serializable {
    private Long id;
    private double totalPrice;
    private boolean fastDelivery;
    private String customer;
    private String itemId;

    public Order() {
    }

    public Long getId() {
        return this.id;
    }

    public double getTotalPrice() {
        return this.totalPrice;
    }

    public boolean isFastDelivery() {
        return this.fastDelivery;
    }

    public String getCustomer() {
        return this.customer;
    }

    public String getItemId() {
        return this.itemId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setFastDelivery(boolean fastDelivery) {
        this.fastDelivery = fastDelivery;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Order)) return false;
        final Order other = (Order) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
        if (Double.compare(this.getTotalPrice(), other.getTotalPrice()) != 0) return false;
        if (this.isFastDelivery() != other.isFastDelivery()) return false;
        final Object this$customer = this.getCustomer();
        final Object other$customer = other.getCustomer();
        if (this$customer == null ? other$customer != null : !this$customer.equals(other$customer)) return false;
        final Object this$itemId = this.getItemId();
        final Object other$itemId = other.getItemId();
        if (this$itemId == null ? other$itemId != null : !this$itemId.equals(other$itemId)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Order;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $id = this.getId();
        result = result * PRIME + ($id == null ? 43 : $id.hashCode());
        final long $totalPrice = Double.doubleToLongBits(this.getTotalPrice());
        result = result * PRIME + (int) ($totalPrice >>> 32 ^ $totalPrice);
        result = result * PRIME + (this.isFastDelivery() ? 79 : 97);
        final Object $customer = this.getCustomer();
        result = result * PRIME + ($customer == null ? 43 : $customer.hashCode());
        final Object $itemId = this.getItemId();
        result = result * PRIME + ($itemId == null ? 43 : $itemId.hashCode());
        return result;
    }

    public String toString() {
        return "Order(id=" + this.getId() + ", totalPrice=" + this.getTotalPrice() + ", fastDelivery=" + this.isFastDelivery() + ", customer=" + this.getCustomer() + ", itemId=" + this.getItemId() + ")";
    }
}
