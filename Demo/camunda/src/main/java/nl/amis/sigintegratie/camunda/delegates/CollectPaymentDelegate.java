package nl.amis.sigintegratie.camunda.delegates;

import nl.amis.sigintegratie.camunda.model.Order;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component
public class CollectPaymentDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        Order order = (Order)delegateExecution.getVariable("order");
        System.out.println("Collecting payment of " + order.getTotalPrice() + "for order " + order.getId() );
    }
}
